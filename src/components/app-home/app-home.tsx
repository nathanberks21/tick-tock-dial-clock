import { Component, Host, h, State } from '@stencil/core';
import { getTimeList } from './app-home.utils';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
  shadow: true,
})
export class AppHome {
  @State() now = new Date();

  componentWillLoad() {
    setInterval(() => {
      this.now = new Date();
    }, 1000);
  }

  render() {
    const hours = this.now.getHours();
    const minutes = this.now.getMinutes();
    const seconds = this.now.getSeconds();

    return (
      <Host>
        <app-dial dialValues={getTimeList(24)} currentDialIndex={hours} />
        <app-dial dialValues={getTimeList(60)} currentDialIndex={minutes} />
        <app-dial dialValues={getTimeList(60)} currentDialIndex={seconds} />
      </Host>
    );
  }

}
