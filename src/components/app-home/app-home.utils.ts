export function getTimeList(intervals: number): string[] {
  return [...Array(intervals)].map((_, idx) => {
    const stringValue = idx.toFixed(0);

    return stringValue.length === 1 ? `0${stringValue}` : stringValue;
  })
}
