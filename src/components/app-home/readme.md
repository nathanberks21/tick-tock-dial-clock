# app-home



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [app-dial](../app-dial)

### Graph
```mermaid
graph TD;
  app-home --> app-dial
  style app-home fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
