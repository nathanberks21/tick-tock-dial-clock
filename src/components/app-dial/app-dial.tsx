import { Component, Host, h, Prop, Watch, Listen } from '@stencil/core';

@Component({
  tag: 'app-dial',
  styleUrl: 'app-dial.css',
  shadow: true,
})
export class AppDial {
  @Prop() dialValues: string[];
  @Prop() currentDialIndex: number;

  private isFirstLoad = true;
  private hasRegainedFocus = false;
  private currentRotation: number;

  @Watch('currentDialIndex')
  onCurrentDialIndexChange() {
    if (this.hasRegainedFocus) {
      this.calculateInitialRotation();

      this.hasRegainedFocus = false;

      return;
    }

    const rotationIntervals = 360 / this.dialValues.length;

    this.currentRotation += rotationIntervals;
  }

  @Listen('visibilitychange', { target: 'window' })
  onVisibilityChange() {
    this.hasRegainedFocus = true;

  }

  componentWillLoad() {
    this.calculateInitialRotation();
  }

  componentDidLoad() {
    this.isFirstLoad = false;
  }

  calculateInitialRotation() {
    const rotationIntervals = 360 / this.dialValues.length;

    this.currentRotation = rotationIntervals * this.currentDialIndex;
  }

  renderDialValue(dialValue: string, dialIdx: number) {
    const rotationIntervals = 360 / this.dialValues.length;
    const rotation = rotationIntervals * dialIdx;

    return <div class="dial-value" style={{
      transform: `rotate(${rotation}deg)`
    }}>{dialValue}</div>;
  }

  renderCurrentDialValue() {
    return <div class="current-dial-value">{this.dialValues[this.currentDialIndex]}</div>
  }

  render() {
    if (!this.dialValues.length || this.currentDialIndex === undefined) {
      return;
    }

    const currentRotation = this.currentRotation * -1;

    return (
      <Host>
        <div class="current-dial-value-indicator"></div>
        <div
          class={{
            'dial-container': true,
            'has-loaded': !this.isFirstLoad
          }}
          style={{
            transform: `rotate(${currentRotation}deg)`
          }}>
          {this.dialValues.map((value, idx) => this.renderDialValue(value, idx))}
        </div>
        {this.renderCurrentDialValue()}
      </Host>
    );
  }
}
