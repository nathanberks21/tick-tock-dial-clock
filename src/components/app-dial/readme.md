# app-dial



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description | Type       | Default     |
| ------------------ | -------------------- | ----------- | ---------- | ----------- |
| `currentDialIndex` | `current-dial-index` |             | `number`   | `undefined` |
| `dialValues`       | --                   |             | `string[]` | `undefined` |


## Dependencies

### Used by

 - [app-home](../app-home)

### Graph
```mermaid
graph TD;
  app-home --> app-dial
  style app-dial fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
